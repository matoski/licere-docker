#!/bin/sh
set -e

if [ "$1" = 'licere-server' ]; then
    shift
    exec licere-server \
        --licere-certificate=/certs/licere.cert.pem \
        --licere-key=/certs/licere.key.pem \
        --host=0.0.0.0 \
        --tls-port=443 \
        --tls-ca=/certs/ca-chain.cert.pem \
        --tls-key=/certs/server.key.pem \
        --tls-certificate=/certs/server.cert.pem \
        --dns="/data/licere.db?cache=shared&_loc=UTC&_fk=on" \
        "$@"
elif [ "$1" = 'licere' ]; then
    shift
    exec licere \
        --tls-ca=/certs/ca-chain.cert.pem \
        --tls-key=/certs/client.key.pem \
        --tls-certificate=/certs/client.cert.pem \
        "$@"
fi

exec "$@"
