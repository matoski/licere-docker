# Supported tags and respective `Dockerfile` links

* [latest, 1.0.0](https://bitbucket.org/matoski/licere-docker/Dockerfile)

# Quick reference

* Maintained by: [MATOSKI LTD](https://www.matoski.co.uk)
* Where to get help: [Licere](https://www.matoski.co.uk/products/licere), [FAQ](https://www.matoski.co.uk/products/licere/faq), [support@matoski.co.uk](mailto:support@matoski.co.uk)
* Where to file issues: [Licere Issues](https://bitbucket.org/matoski/licere-docker/issues)
* Maintained by: [MATOSKI LTD](http://www.matoski.co.uk)

# What's Licere Server?

Licere server is a simple licensing solution for your software. It supports flexible licensing, as well as node-locked licensing (locked to the machine where the license is activated).

## Licere CLI Tool

Licere CLI tool allows you to sign-up and manage your Licere licenses securely and with ease.

# Quick Start with Licere and Docker

You will need a private key and Licere product certificate to securely communicate with Licere server and authenticate yourself.

## Step 1: Create Your Private Key

We use RSA private key, so if you already have one you can use that and skip this step.

You will need a private key and Licere product certificate to securely communicate with Licere server and authenticate yourself. We use RSA private key, so if you already have one you can use that and skip this step.

To create private key we will use the `licere` CLI tool.

```sh
mkdir -p ./certs
docker run -it --rm matoski/licere licere gen key | tee ./certs/licere.key.pem
```

This will print the private key like this and save under `./certs/licere.key.pem`.

```sh
-----BEGIN PRIVATE KEY-----
MIIEowIBAAKCAQEAv0X/W8UacTcIDPJH7+Fqn3UHhdOXu2lVtRBoxto/YQc8kD3g
u6CdxvytD9NyN6Z8TjlYnPKR5WtAyWpy78a/GcQvyGDVaNwLv4+l7GSzYqWCUkTK
HuGSmk1PYE5XgXDAig3wtJ++ZFxWmb2s/KEgRpLc8ssrgtltntVfdyLVc8k1pfgv
...
smJuHL8KaXYGvYZI31vWwXJx1mx991Mfxap98irPsUzk1eVB+S0Q
-----END PRIVATE KEY-----
```

## Step 2: Create Your Product Certificate Signing Request (CSR)

You will need separate Licere certificate for each product you want to license with Licere. Licere certificate is a normal certificate signed by MATOSKI LTD with the access rights and product information embedded as certificate extensions.

* `1.6.2.8.6.7.5.4.542373.1` - access right
* `1.6.2.8.6.7.5.4.542373.2` - product ID

Once you've generated the CSR using your private key use the [Product Registration](https://www.matoski.co.uk/products/licere/signup) form.

To generate your CSR replace the sample text with your real details in the command below:

```sh
cat ./certs/licere.key.pem \
| docker run -i --rm matoski/licere licere gen csr \
    --organization="Full name or Company name" \
    --country=GB \
    --email="your@email.com" \
| tee ./certs/your-product.csr.pem
```

MATOSKI LTD will examine your request received by submitting the [Product Registration](https://www.matoski.co.uk/products/licere/signup) form and email your signed Product Certificate populated with the right access and product ID.

# License Terms

This software requires an active [Licere license](https://www.matoski.co.uk/products/licere/signup).

Copyright (c) 2020 MATOSKI LTD. All rights reserved.
