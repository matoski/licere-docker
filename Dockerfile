FROM matoski/licere-server:latest

LABEL Name="Licere Simple Licensing" Version="1.0.0" Maintainer="MATOSKI LTD <info@matoski.co.uk>"

WORKDIR /usr/local/licere

COPY scripts/entrypoint.sh /usr/local/licere/docker-entrypoint.sh

VOLUME [ "/data", "/certs" ]

ENTRYPOINT [ "/usr/local/licere/docker-entrypoint.sh" ]
CMD [ "licere-server" ]

EXPOSE 443
